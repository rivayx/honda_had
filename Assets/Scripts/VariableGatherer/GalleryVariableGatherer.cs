﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GalleryVariableGatherer : MonoBehaviour
{
    public string URL { get; set; }
    public string UserId { get; set; }
    public string Date { get; set; }
}
