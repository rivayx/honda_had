﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UploadPanelControl : MonoBehaviour
{
    [SerializeField] Button _Upload;
    [SerializeField] Image _Preview;

    private byte[] bytesPhoto;

    void OnEnable()
    {
        NativeToolkit.OnImagePicked += ImagePicked;
        _Upload.onClick.AddListener(UploadAction);
        _Preview.GetComponent<Button>().onClick.AddListener(PickImage);
    }

    private void PickImage()
    {
        NativeToolkit.PickImage();
    }

    void ImagePicked(Texture2D img, string path)
    {

        _Preview.sprite = StaticFunction.Texture2DToSprite(img);
        bytesPhoto = img.EncodeToPNG();

        //StartCoroutine(GetImage("file://" + path));
        //Destroy(img);
    }

    private IEnumerator GetImage(string path)
    {
        MessageModal.Instance().OpenMessage(path);
        WWW www = new WWW(path);
        yield return www;

        _Preview.sprite = StaticFunction.Texture2DToSprite(www.texture);
        bytesPhoto = www.texture.EncodeToJPG(50);
    }

    private void UploadAction()
    {
        GetPostData.UploadGallery(bytesPhoto, 0, JSONActionUploadPhoto);
    }

    private void JSONActionUploadPhoto(string jsonString)
    {
        Singleton.Instance.Loading = false;
        Debug.Log("Upload Photo : " + jsonString);

        JSONNode upload = JSONNode.Parse(jsonString);

        bool success = upload["status"].AsBool;
        if (success)
        {
            MessageModal.Instance().OpenMessage("Sukses mengupload foto");
            _Preview.sprite = StaticFunction.Texture2DToSprite(StaticFunction.LoadTextureResource("click_image"));
            bytesPhoto = null;
        }
        else
        {
            MessageModal.Instance().OpenMessage(upload["data"]);
        }
    }

    void OnDisable()
    {
        NativeToolkit.OnImagePicked -= ImagePicked;
        _Preview.GetComponent<Button>().onClick.RemoveAllListeners();
        _Upload.onClick.RemoveAllListeners();
    }
}
