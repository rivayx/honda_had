﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraPanelControl : MonoBehaviour
{
    [SerializeField] Button _Upload;
    [SerializeField] Button _ReTake;

    [SerializeField] RawImage _Preview;

    private byte[] bytesPhoto;
    private bool uploaded = true;

    void OnEnable()
    {
        NativeToolkit.OnCameraShotComplete += CameraShotComplete;
        
        if (uploaded)
            RetakeAction();

        RegisterModal();
    }


    private void RegisterModal()
    {
        _Upload.onClick.AddListener(UploadAction);
        _ReTake.onClick.AddListener(RetakeAction);
    }

    private void RetakeAction()
    {
        _Preview.texture = StaticFunction.LoadTextureResource("logo");
        NativeToolkit.TakeCameraShot();
    }


    void CameraShotComplete(Texture2D img, string path)
    {
        uploaded = false;
        _Preview.texture = img;
        bytesPhoto = img.EncodeToPNG();
        //StartCoroutine(GetImage("file://" + path));
        //Destroy(img);
    }

    private IEnumerator GetImage(string path)
    {
        MessageModal.Instance().OpenMessage(path);
        WWW www = new WWW(path);
        yield return www;

        _Preview.texture = www.texture;
        bytesPhoto = www.texture.EncodeToJPG(50);
    }

    private void UploadAction()
    {
        GetPostData.UploadGallery(bytesPhoto, 0, JSONActionUploadPhoto);
    }

    private void JSONActionUploadPhoto(string jsonString)
    {
        Singleton.Instance.Loading = false;
        Debug.Log("Upload Photo : " + jsonString);

        JSONNode upload = JSONNode.Parse(jsonString);

        bool success = upload["status"].AsBool;
        if (success)
        {
            MessageModal.Instance().OpenMessage("Sukses mengupload foto");
            uploaded = true;
        }
        else
        {
            MessageModal.Instance().OpenMessage(upload["data"]);
        }
    }

    private void UnregisterModal()
    {
        _ReTake.onClick.RemoveAllListeners();
        _Upload.onClick.RemoveAllListeners();
    }

    void OnDisable()
    {
        UnregisterModal();
        NativeToolkit.OnCameraShotComplete -= CameraShotComplete;
        UnregisterModal();
    }
}