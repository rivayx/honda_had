﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GalleryPanelControl : MonoBehaviour
{
    [SerializeField] RectTransform _GalleryPlaceHolder;

    private List<GameObject> ListGalleryHolderObject = new List<GameObject>();

    private int totalItemInPage = 12;

    private float totalGeser;
    private float setengahGeser;
    private int IndexShow;

    public void OpenGallery()
    {
        _GalleryPlaceHolder.gameObject.SetActive(false);
        _GalleryPlaceHolder.GetComponent<HolderGalleryVariableGatherer>().Item.SetActive(false);

        totalGeser = -(_GalleryPlaceHolder.parent.GetComponent<HorizontalLayoutGroup>().spacing + _GalleryPlaceHolder.sizeDelta.x);
        setengahGeser = totalGeser / 2;
        Singleton.Instance.Loading = true;

        GetPostData.ListGallery(JSONActionGallery);
    }

    private void JSONActionGallery(string jsonString)
    {
        //Debug.Log("GalleryList ::" + jsonString);

        JSONParsing baseJSON = new GetGallery(jsonString);

        StartCoroutine(CreateProductUI(baseJSON.ParseJSON()));
    }
    
    private IEnumerator CreateProductUI(List<ItemClass> listProduct)
    {
        listProduct.Reverse();
        int totalPageCreated = listProduct.Count % totalItemInPage == 0 ?
                               listProduct.Count / totalItemInPage :
                               (listProduct.Count / totalItemInPage) + 1;
        int galleryNumber = 0;

        for (int i = 0; i < totalPageCreated; i++)
        {
            yield return new WaitForEndOfFrame();

            GameObject newHolder = Instantiate(_GalleryPlaceHolder.gameObject, _GalleryPlaceHolder.parent);
            newHolder.name = "GalleryHolder-" + i;
            newHolder.SetActive(true);
            HolderGalleryVariableGatherer holderVariable = newHolder.GetComponent<HolderGalleryVariableGatherer>();

            int totalCreateItemInHolder = i == totalPageCreated - 1 ? 
                                          (
                                              listProduct.Count % totalItemInPage == 0 ?
                                              totalItemInPage : listProduct.Count % totalItemInPage 
                                          ):
                                          totalItemInPage;

            
            for (int j = 0; j < totalCreateItemInHolder; j++)
            {
                yield return new WaitForEndOfFrame();

                GameObject newItem = Instantiate(holderVariable.Item, holderVariable.transform);
                newItem.name = "Gallery-" + galleryNumber;
                newItem.SetActive(true);
                //newItem.GetComponent<Image>().sprite = StaticFunction.GenerateSpriteFromURLResource("had_loading");

                GalleryItemClass galleryItemClass = listProduct[galleryNumber] as GalleryItemClass;
                GalleryVariableGatherer galleryVariable = newItem.GetComponent<GalleryVariableGatherer>();

                galleryVariable.URL = galleryItemClass.URL;
                galleryVariable.UserId = galleryItemClass.UserId;
                galleryVariable.Date = galleryItemClass.Date;

                galleryNumber++;

                holderVariable.ListGalleryItemObject.Add(newItem);
            }

            ListGalleryHolderObject.Add(newHolder);
        }
        Singleton.Instance.Loading = false;
        LoadImageInPage();
    }

    private void LoadImageInPage()
    {
        if (ListGalleryHolderObject.Count > 0)
        {
            foreach(GameObject holder in ListGalleryHolderObject)
            {
                HolderGalleryVariableGatherer holderVeriable = holder.GetComponent<HolderGalleryVariableGatherer>();

                if (holder == ListGalleryHolderObject[IndexShow])
                {
                    foreach (GameObject item in holderVeriable.ListGalleryItemObject)
                    {
                        ImageGetter.GetImage().StartGettingImage(item.GetComponent<Image>(), item.GetComponent<GalleryVariableGatherer>().URL);
                    }
                }
                else
                {
                    foreach (GameObject item in holderVeriable.ListGalleryItemObject)
                    {
                        item.GetComponent<Image>().sprite = StaticFunction.LoadSpriteResource("had_loading");
                    }
                }
            }
        }
    }

    void Update()
    {
        if (ListGalleryHolderObject.Count > 0)
        {
            if ((totalGeser * IndexShow) + setengahGeser > _GalleryPlaceHolder.parent.localPosition.x)
            {
                IndexShow++;
                LoadImageInPage();
            }

            if ((totalGeser * IndexShow) - setengahGeser < _GalleryPlaceHolder.parent.localPosition.x)
            {
                IndexShow--;
                LoadImageInPage();
            }

            float anchoredX = Mathf.Lerp(_GalleryPlaceHolder.parent.GetComponent<RectTransform>().anchoredPosition.x,
                                         -(_GalleryPlaceHolder.parent.GetComponent<HorizontalLayoutGroup>().spacing + _GalleryPlaceHolder.sizeDelta.x) * IndexShow,
                                         Time.deltaTime * 20);

            if (Singleton.Instance.startLerp)
                _GalleryPlaceHolder.parent.GetComponent<RectTransform>().anchoredPosition = new Vector2(anchoredX, _GalleryPlaceHolder.parent.GetComponent<RectTransform>().anchoredPosition.y);

        }
    }

    public void CloseGallery()
    {
        if (ListGalleryHolderObject.Count > 0)
        {
            foreach (GameObject go in ListGalleryHolderObject)
            {
                Destroy(go);
            }
            ListGalleryHolderObject.Clear();
        }

    }
}
