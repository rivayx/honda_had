﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseControl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public void OnPointerDown(PointerEventData pointerEventData)
    {
        Singleton.Instance.startLerp = false;
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
        Singleton.Instance.startLerp = true;
    }
}
