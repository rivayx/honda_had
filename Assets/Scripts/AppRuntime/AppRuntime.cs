﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class AppRuntime : BaseRuntime
{
    [SerializeField] GameObject _PanelLoading;

    void Awake()
    {
        if (Singleton.Instance == null)
        {
            Singleton.Instance = gameObject.AddComponent<Singleton>();
            DontDestroyOnLoad(gameObject);

            Singleton.Instance.PanelLoading = _PanelLoading;
        }
    }

    void Start()
    {
        MakeFSM();
    }

    protected override void MakeFSM()
    {
        InitState init = new InitState();
        init.AddTRANSITION(Transition.ToSplashState, StateID.SplashState);

        SplashState splash = new SplashState();
        splash.AddTRANSITION(Transition.ToLoginState, StateID.LoginState);

        LoginState login = new LoginState();
        login.AddTRANSITION(Transition.ToHomeState, StateID.HomeState);

        HomeState home = new HomeState();
        home.AddTRANSITION(Transition.ToLoginState, StateID.LoginState);
        home.AddTRANSITION(Transition.ToBarcodeState, StateID.BarcodeState);
        home.AddTRANSITION(Transition.ToTulipState, StateID.TulipState);
        home.AddTRANSITION(Transition.ToProfileState, StateID.ProfileState);
        home.AddTRANSITION(Transition.ToRundownState, StateID.RundownState);
        home.AddTRANSITION(Transition.ToRulesState, StateID.RulesState);
        home.AddTRANSITION(Transition.ToGalleryState, StateID.GalleryState);
        home.AddTRANSITION(Transition.ToEmergencyState, StateID.EmergencyState);

        TulipState tulip = new TulipState();
        tulip.AddTRANSITION(Transition.ToHomeState, StateID.HomeState);
        tulip.AddTRANSITION(Transition.ToBarcodeState, StateID.BarcodeState);

        ProfileState profile = new ProfileState();
        profile.AddTRANSITION(Transition.ToHomeState, StateID.HomeState);
        profile.AddTRANSITION(Transition.ToChangePassword, StateID.ChangePassword);

        BarcodeState barcode = new BarcodeState();
        barcode.AddTRANSITION(Transition.ToHomeState, StateID.HomeState);
        barcode.AddTRANSITION(Transition.ToTulipState, StateID.TulipState);

        EmergencyState emergency = new EmergencyState();
        emergency.AddTRANSITION(Transition.ToHomeState, StateID.HomeState);

        GalleryState gallery = new GalleryState();
        gallery.AddTRANSITION(Transition.ToHomeState, StateID.HomeState);

        ChangePasswordState password = new ChangePasswordState();
        password.AddTRANSITION(Transition.ToProfileState, StateID.ProfileState);

        RulesState rules = new RulesState();
        rules.AddTRANSITION(Transition.ToHomeState, StateID.HomeState);

        RundownState rundown = new RundownState();
        rundown.AddTRANSITION(Transition.ToHomeState, StateID.HomeState);

        _FSM = new FSMSystem(this);
        //_FSM.AddState(init);
        _FSM.AddState(splash);
        _FSM.AddState(login);
        _FSM.AddState(home);
        _FSM.AddState(barcode);
        _FSM.AddState(emergency);
        _FSM.AddState(gallery);
        _FSM.AddState(profile);
        _FSM.AddState(password);
        _FSM.AddState(rules);
        _FSM.AddState(rundown);
        _FSM.AddState(tulip);

        base.MakeFSM();
    }
}
