﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;
using System.Globalization;

public class StaticFunction
{
    public static Sprite LoadSpriteResource(string ImageURL)
    {
        Sprite result = null;
        string imagePath = "Images/" + ImageURL;
        result = Resources.Load<Sprite>(imagePath);

        return result;
    }

    public static Texture2D LoadTextureResource(string ImageURL)
    {
        Texture2D result = null;
        string imagePath = "Images/" + ImageURL;
        result = Resources.Load<Texture2D>(imagePath);

        return result;
    }

    public static Sprite Texture2DToSprite(Texture2D tex)
    {
        return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
    }



    public static string FormatCurrency(int value)
    {
        return "Rp " + FormatGeneral(value);
    }

    public static string FormatGeneral(int value)
    {
        return FormatNumber("N0", value);
    }

    public static string FormatNumber(string format, int value)
    {
        CultureInfo id = new CultureInfo("id-ID");

        string val = value.ToString(format, id);

        return val;
    }

    public static int CountFileTulip(Class className, int stage)
    {
        int result = 0;

        switch (className)
        {
            case Class.BigWing:

                if (stage == 1)
                    result = 40;
                else if (stage == 2)
                    result = 41;

                break;
            case Class.Wing:

                if (stage == 1)
                    result = 42;
                else if (stage == 2)
                    result = 44;

                break;
        }

        return result;
    }

    public static Sprite LoadTulipSprite(Class className, int stage, int page)
    {
        string path = "Images/Tulip/";

        switch (className)
        {
            case Class.Wing:
                path += "WING-" + stage.ToString() + "/HAD 2019 Roadbook DUAL PURPOSE - STAGE " + stage.ToString();
                break;
            case Class.BigWing:
                path += "BIG WING-" + stage.ToString() + "/HAD 2019 Roadbook BIGBIKE - STAGE " + stage.ToString();
                break;
        }

        return Resources.Load<Sprite>(path + "-" + page.ToString("D2"));
    }

    public static Sprite LoadRulesSprite(int page)
    {
        string path = "Images/Rules/Rules HAD 2019-" + page.ToString("D2");

        return Resources.Load<Sprite>(path);
    }
}