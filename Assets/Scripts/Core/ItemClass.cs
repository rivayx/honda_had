﻿using BaseApps;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemClass
{

}

public class GalleryItemClass : ItemClass
{
    public string URL { get; set; }
    public string UserId { get; set; }
    public string Date { get; set; }

    public GalleryItemClass(string url, string userid, string date)
    {
        URL = url;
        UserId = userid;
        Date = date;
    }
}