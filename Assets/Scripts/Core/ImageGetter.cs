﻿using System;
using System.Collections;
using System.Collections.Generic;
using BaseApps;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ImageGetter : MonoBehaviour
{
    public class ImageSprintEvent : UnityEvent<List<Sprite>>
    {

    }

    private WWW _URLLink;

    public static ImageGetter GetImage()
    {
        GameObject go = new GameObject("LoadingImage");
        go.AddComponent<ImageGetter>();
        ImageGetter image = go.GetComponent<ImageGetter>() as ImageGetter;

        return image;
    }
    
    #region Load Sprite
    public void StartGettingImage(Image image, string urlLink)
    {
        try
        {
            StartCoroutine(StartGettingImageCoroutine(image, urlLink));
        }
        catch (Exception e)
        {
            Debug.LogError(e.StackTrace);
            Destroy(gameObject);
        }
    }
    private IEnumerator StartGettingImageCoroutine(Image image, string urlString)
    {
        image.sprite = StaticFunction.LoadSpriteResource("had_loading");
        WWW url = new WWW(urlString);
        yield return url;

        try
        {
            if (url.error != null)
            {
                image.sprite = StaticFunction.LoadSpriteResource("no_image");
                Destroy(gameObject);
            }
            else
            {
                image.sprite = StaticFunction.Texture2DToSprite(url.texture);
                Destroy(gameObject);
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e.StackTrace);
            Destroy(gameObject);
        }
    }
    #endregion

    void OnDestroy()
    {
        _URLLink = null;
    }
}