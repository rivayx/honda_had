﻿using System;
using BaseApps;
using UnityEngine;
using UnityEngine.Events;


public class JSONGetter : MonoBehaviour
{
    private WWW _URLLink;
    private JSONEvent _JSONEventCaller;
    private float progress = 0;

    public static JSONGetter GetJSON()
    {
        GameObject json = new GameObject("JSONGetter");
        json.AddComponent<JSONGetter>();

        JSONGetter getter = json.GetComponent<JSONGetter>() as JSONGetter;
        return getter;
    }
    
    public void StartParsing(string urlLink, UnityAction<string> unityAction)
    {
        _URLLink = new WWW(urlLink);
        EventJSON(unityAction);
    }

    public void StartParsing(string urlLink, WWWForm form, UnityAction<string> unityAction)
    {
        _URLLink = new WWW(urlLink, form);
        EventJSON(unityAction);
    }

    private void EventJSON(UnityAction<string> unityAction)
    {
        _JSONEventCaller = new JSONEvent();
        _JSONEventCaller.AddListener(unityAction);
    }
    
    void FixedUpdate()
    {
        try
        {
            if (_URLLink != null)
            {
                if (_URLLink.isDone)
                {
                    if (_URLLink.error != null)
                    {
                        _JSONEventCaller.Invoke(_URLLink.error + " - " + _URLLink.url);
                        Debug.LogError(_URLLink.error + " - " + _URLLink.url);
                        Destroy(gameObject);
                    }
                    else
                    {
                        _JSONEventCaller.Invoke(_URLLink.text);
                        Destroy(gameObject);
                    }
                }
            }
        }
        catch(Exception e)
        {
            Debug.LogError(e);
            Destroy(gameObject);
        }
    }

    void OnDestroy()
    {
        _JSONEventCaller.RemoveAllListeners();
        _JSONEventCaller = null;
        _URLLink = null;
    }
}