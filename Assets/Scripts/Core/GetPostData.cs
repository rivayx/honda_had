﻿using BaseApps;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GetPostData
{
    public static void Login(string email, string password, UnityAction<string> unityAction)
    {
        WWWForm login = new WWWForm();
        JSONGetter getJSON = JSONGetter.GetJSON();

        login.AddField("uname", email);
        login.AddField("pwd", password);

        getJSON.StartParsing(URL.LOGIN, login, unityAction);
        return;
    }

    public static void Logout(UnityAction<string> unityAction)
    {
        WWWForm logout = new WWWForm();
        JSONGetter getJSON = JSONGetter.GetJSON();

        logout.AddField("key", Singleton.Instance.Key);

        getJSON.StartParsing(URL.LOGOUT, logout, unityAction);
        return;
    }

    public static void UserInfo(UnityAction<string> unityAction)
    {
        WWWForm logout = new WWWForm();
        JSONGetter getJSON = JSONGetter.GetJSON();

        logout.AddField("key", Singleton.Instance.Key);

        getJSON.StartParsing(URL.USER_INFO, logout, unityAction);
        return;
    }

    public static void ParticipantInfo(UnityAction<string> unityAction)
    {
        WWWForm logout = new WWWForm();
        JSONGetter getJSON = JSONGetter.GetJSON();

        logout.AddField("key", Singleton.Instance.Key);

        getJSON.StartParsing(URL.PARTICIPANT_INFO, logout, unityAction);
        return;
    }

    public static void SendLocation(UnityAction<string> unityAction)
    {
        WWWForm location = new WWWForm();
        JSONGetter getJSON = JSONGetter.GetJSON();

        float latitude = Input.location.lastData.latitude;
        float longitude = Input.location.lastData.longitude;

        location.AddField("key", Singleton.Instance.Key);
        location.AddField("lt", latitude.ToString());
        location.AddField("lg", longitude.ToString());

        getJSON.StartParsing(URL.COORDINAT_ADD, location, unityAction);
        return;
    }

    public static void ScanPanitia(string scanData, UnityAction<string> unityAction)
    {
        WWWForm scan = new WWWForm();
        JSONGetter getJSON = JSONGetter.GetJSON();

        int status = 0;
        switch (Singleton.Instance.BarcodeType)
        {
            case ScanBarcodeType.Start1:
                status = 10;
                break;
            case ScanBarcodeType.Finish1:
                status = 20;
                break;
            case ScanBarcodeType.Start2:
                status = 30;
                break;
            case ScanBarcodeType.Finish2:
                status = 40;
                break;
        }

        scan.AddField("key", Singleton.Instance.Key);
        scan.AddField("status", status);
        scan.AddField("scandata", scanData);

        getJSON.StartParsing(URL.SCAN_PANITIA, scan, unityAction);
        return;
    }

    public static void ScanPeserta(string scanData, UnityAction<string> unityAction)
    {
        WWWForm scan = new WWWForm();
        JSONGetter getJSON = JSONGetter.GetJSON();

        scan.AddField("key", Singleton.Instance.Key);
        scan.AddField("scandata", scanData);
        scan.AddField("startumber", Singleton.Instance.Number);

        getJSON.StartParsing(URL.SCAN_PESERTA, scan, unityAction);
        return;
    }

    public static void ScanStatus(ScanBarcodeType barcodeType, UnityAction<string> unityAction)
    {
        WWWForm scan = new WWWForm();
        JSONGetter getJSON = JSONGetter.GetJSON();

        int status = 0;
        switch (barcodeType)
        {
            case ScanBarcodeType.ReRegister:
                status = 5;
                break;
            case ScanBarcodeType.Start1:
                status = 10;
                break;
            case ScanBarcodeType.Finish1:
                status = 20;
                break;
            case ScanBarcodeType.Start2:
                status = 30;
                break;
            case ScanBarcodeType.Finish2:
                status = 40;
                break;
        }

        scan.AddField("key", Singleton.Instance.Key);
        scan.AddField("status", status);
        scan.AddField("startumber", Singleton.Instance.Number);

        getJSON.StartParsing(URL.SCAN_STATUS, scan, unityAction);
        return;
    }

    public static void ScanLastStatus(UnityAction<string> unityAction)
    {
        WWWForm scan = new WWWForm();
        JSONGetter getJSON = JSONGetter.GetJSON();

        scan.AddField("key", Singleton.Instance.Key);
        scan.AddField("startnumber", Singleton.Instance.Number);

        getJSON.StartParsing(URL.SCAN_LAST_STATUS, scan, unityAction);
        return;
    }

    public static void ChangePassword(string oldPasswd, string newPasswd, string confirmPasswd, UnityAction<string> unityAction)
    {

        Singleton.Instance.Loading = true;
        WWWForm passwd = new WWWForm();
        JSONGetter getJSON = JSONGetter.GetJSON();

        passwd.AddField("key", Singleton.Instance.Key);
        passwd.AddField("oldpass", oldPasswd);
        passwd.AddField("newpass", newPasswd);
        passwd.AddField("cnewpass", confirmPasswd);

        getJSON.StartParsing(URL.CHANGE_PASSWORD, passwd, unityAction);
        return;
    }

    public static void UploadGallery(byte[] image, int stage, UnityAction<string> unityAction)
    {
        Singleton.Instance.Loading = true;

        WWWForm gallery = new WWWForm();
        JSONGetter getJSON = JSONGetter.GetJSON();

        gallery.AddField("key", Singleton.Instance.Key);
        gallery.AddBinaryData("image", image);

        int status = stage == 1 ? 20 : 40;
        gallery.AddField("status", status);

        getJSON.StartParsing(URL.UPLOAD_GALERY, gallery, unityAction);
        return;
    }


    public static void UploadGallery(byte[] image, string result, UnityAction<string> unityAction)
    {
        Singleton.Instance.Loading = true;

        WWWForm gallery = new WWWForm();
        JSONGetter getJSON = JSONGetter.GetJSON();

        gallery.AddField("key", Singleton.Instance.Key);
        gallery.AddBinaryData("image", image);
        gallery.AddField("status", result);

        getJSON.StartParsing(URL.UPLOAD_GALERY, gallery, unityAction);
        return;
    }

    public static void ListGallery(UnityAction<string> unityAction)
    {
        WWWForm gallery = new WWWForm();
        JSONGetter getJSON = JSONGetter.GetJSON();

        gallery.AddField("key", Singleton.Instance.Key);
        //gallery.AddField("offset", 0);
        //gallery.AddField("count", 15);

        getJSON.StartParsing(URL.LIST_GALLERY, gallery, unityAction);
        return;
    }


    public static void RoadBook(UnityAction<string> unityAction)
    {
        WWWForm roadbook = new WWWForm();
        JSONGetter getJSON = JSONGetter.GetJSON();

        roadbook.AddField("key", Singleton.Instance.Key);

        getJSON.StartParsing(URL.ROAD_BOOK, roadbook, unityAction);
        return;
    }

    public static void GiveUp(int stage, UnityAction<string> unityAction)
    {
        WWWForm giveup = new WWWForm();
        JSONGetter getJSON = JSONGetter.GetJSON();
        int status = stage == 1 ? 10 : 30;

        giveup.AddField("key", Singleton.Instance.Key);
        giveup.AddField("status", stage);

        getJSON.StartParsing(URL.ROAD_BOOK, giveup, unityAction);
        return;
    }

}
