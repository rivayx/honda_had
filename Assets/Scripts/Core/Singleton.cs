﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BaseApps;
using SimpleJSON;

public class Singleton : MonoBehaviour
{
    public StateID ActiveState { get; set; }

    public GameObject PanelLoading { get; set; }


    public bool Loading { get; set; }
    private GameObject NewPanelLoading;

    //USER
    public string ParticipantName { get; set; }
    public bool ChangePass { get; set; }
    public string Key { get; set; }
    public string Number { get; set; }
    public Role UserType { get; set; }
    public Class Class { get; set; }
    public string MobilePhone { get; set; }
    public string Email { get; set; }

    public bool startLerp { get; set; }

    public ScanBarcodeType LastScanBarcode { get; set; }
    public ScanBarcodeType BarcodeType { get; set; }

    private bool SendingLocation;
    private float TimeSendLocation;

    public static Singleton Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

            if (Instance == null)
            {
                Instantiate(Instance);
                Debug.Log("error when trying to create singleton");
            }
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (Loading)
        {
            if (NewPanelLoading == null)
            {
                NewPanelLoading = Instantiate(PanelLoading, FindObjectOfType<Canvas>().transform);
                NewPanelLoading.transform.SetAsLastSibling();
            }
        }
        else
        {
            Destroy(NewPanelLoading);
        }

        if (!SendingLocation && !string.IsNullOrEmpty(Key) && UserType == Role.Peserta)
        {
            TimeSendLocation += Time.deltaTime;
            if (TimeSendLocation > 5f)
            {
                SendingLocation = true;
#if UNITY_EDITOR
                JSONActionLocation("");
#else
                GetPostData.SendLocation(JSONActionLocation);
#endif
            }
        }
    }

    private void JSONActionLocation(string jsonString)
    {
        //Debug.Log("JSON Location ::\n" + jsonString);
        SendingLocation = false;
        TimeSendLocation = 0;
    }

}
