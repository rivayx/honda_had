﻿using System;
using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;

public class GetGallery : JSONParsing
{
    public GetGallery(string JSONPath) : base(JSONPath) { }

    protected override List<ItemClass> JSONObjectToItems(JSONNode jsonNode)
    {
        List<ItemClass> itemClass = new List<ItemClass>();

        for (int i = 0; i < jsonNode["img"].Count; i++)
        {
            JSONNode node = jsonNode["img"][i];

            string url = node["filename"];
            string userid = node["userid"];
            string date = node["createdDtm"];

            ItemClass item = new GalleryItemClass(url, userid, date);
            itemClass.Add(item);
        }
        return itemClass;
    }
}