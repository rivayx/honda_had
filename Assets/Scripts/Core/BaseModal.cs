﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BaseModal : MonoBehaviour
{
    [SerializeField]
    protected GameObject _ModalPanel;

    public bool isOpenModal { get { return _ModalPanel.activeSelf; } }

    void Start()
    {
        CloseModal();
    }

    public virtual void OpenModal()
    {
        _ModalPanel.SetActive(true);
    }

    void Update()
    {
        if (isOpenModal)
        {
            Loop();
        }
    }

    protected virtual void Loop()
    {

    }

    public virtual void CloseModal()
    {
        FindObjectOfType<EventSystem>().SetSelectedGameObject(null);
        _ModalPanel.SetActive(false);
    }
}
