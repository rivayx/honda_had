﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace BaseApps
{
    public class JSONEvent : UnityEvent<string>
    {

    }

    public struct URL
    {
        private static string SERVER_URL = "http://beta.hondaadventuredays.com/api/";

        public static string LOGIN = SERVER_URL + "login";
        public static string LOGOUT = SERVER_URL + "logout";
        public static string USER_INFO = SERVER_URL + "userinfo";
        public static string PARTICIPANT_INFO = SERVER_URL + "participantinfo";
        public static string COORDINAT_ADD = SERVER_URL + "coordinatadd";
        public static string SCAN_PANITIA = SERVER_URL + "scan";
        public static string SCAN_PESERTA = SERVER_URL + "scanpart";
        public static string SCAN_STATUS = SERVER_URL + "scanstatus";
        public static string SCAN_LAST_STATUS = SERVER_URL + "scanlast";
        public static string CHANGE_PASSWORD = SERVER_URL + "changepass";
        public static string UPLOAD_GALERY = SERVER_URL + "upgal";
        public static string LIST_GALLERY = SERVER_URL + "gallery";
        public static string ROAD_BOOK = SERVER_URL + "roadbook";
        public static string GIVE_UP = SERVER_URL + "give_up";

        public static string REGISTER = "http://hondaadventuredays.com/register";

        public static string LOCATION_STAGE1 = "https://www.google.com/maps?saddr=My+Location&daddr=-7.270806,110.421694";
        public static string LOCATION_STAGE2 = "https://www.google.com/maps?saddr=My+Location&daddr=-7.458889,110.401639";

    }

    public enum Transition
    {
        NullTransition = 0,
        ToLoginState = 1,
        ToProfileState = 2,
        ToHomeState = 3,
        ToRundownState = 4,
        ToRulesState = 5,
        ToGalleryState = 6,
        ToEmergencyState = 7,
        ToBarcodeState = 8,
        ToTulipState = 9,
        ToChangePassword = 10,
        ToSplashState = 11,
        ToInitState = 12,
    }

    public enum StateID
    {
        NullStateID = 0,
        LoginState = 1,
        ProfileState = 2,
        HomeState = 3,
        RundownState = 4,
        RulesState = 5,
        GalleryState = 6,
        EmergencyState = 7,
        BarcodeState = 8,
        TulipState = 9,
        ChangePassword = 10,
        SplashState = 11,
        InitState = 12,
    }

    public enum Role
    {
        Panitia = 2,
        Peserta = 3
    }

    public enum Class
    {
        Wing = 1,
        BigWing = 2
    }

    public enum ScanBarcodeType
    {
        Null = 0,
        ReRegister = 1,
        Bonus = 2,
        Start1 = 3,
        Finish1 = 4,
        Start2 = 5,
        Finish2 = 6,
    }
}
