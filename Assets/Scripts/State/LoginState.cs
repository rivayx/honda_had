﻿using UnityEngine;
using BaseApps;

public class LoginState : FSMState
{
    public LoginState()
    {
        stateID = StateID.LoginState;
    }

    public override void OnEnter()
    {
        LoginModal login = LoginModal.Instance();
        login.OpenModal();
        login.RegisterModal(ToHomeState);

        //Debug.Log(URL.LOCATION_STAGE1);
        //Debug.Log(URL.LOCATION_STAGE2);

        base.OnEnter();
    }


    private void ToHomeState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToHomeState);
    }

    public override void OnLeave()
    {
        LoginModal login = LoginModal.Instance();
        login.UnRegisterModal();
        login.CloseModal();

        base.OnLeave();
    }
}