﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class ChangePasswordState : FSMState
{
    public ChangePasswordState()
    {
        stateID = StateID.ChangePassword;
    }

    public override void OnEnter()
    {
        base.OnEnter();

        ChangePasswordModal modal = ChangePasswordModal.Instance();
        modal.OpenModal();
        modal.RegisterModal(ToProfileState);
    }

    private void ToProfileState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToProfileState);
    }

    public override void OnLeave()
    {
        base.OnLeave();
        ChangePasswordModal modal = ChangePasswordModal.Instance();
        modal.CloseModal();
    }
}
