﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class EmergencyState : FSMState
{
    public EmergencyState()
    {
        stateID = StateID.EmergencyState;
    }

    public override void OnEnter()
    {
        EmergencyModal modal = EmergencyModal.Instance();
        modal.OpenModal();
        modal.RegisterModal(ToHomeState);
    }

    private void ToHomeState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToHomeState);
    }

    public override void OnLeave()
    {
        EmergencyModal modal = EmergencyModal.Instance();
        modal.CloseModal();

        base.OnLeave();
    }
}