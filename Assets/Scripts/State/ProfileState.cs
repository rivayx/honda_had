﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class ProfileState : FSMState
{
    public ProfileState()
    {
        stateID = StateID.ProfileState;
    }

    public override void OnEnter()
    {
        base.OnEnter();

        ProfileModal modal = ProfileModal.Instance();
        modal.OpenModal();
        modal.RegisterModal(ToHomeState, ToChangePasswordState);
    }

    private void ToHomeState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToHomeState);
    }

    private void ToChangePasswordState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToChangePassword);
    }

    public override void OnLeave()
    {
        base.OnLeave();
        ProfileModal modal = ProfileModal.Instance();
        modal.CloseModal();
    }
}