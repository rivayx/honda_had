﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class BarcodeState : FSMState
{
    public BarcodeState()
    {
        stateID = StateID.BarcodeState;
    }

    public override void OnEnter()
    {
        base.OnEnter();

        BarcodeModal barcode = BarcodeModal.Instance();
        barcode.OpenModal();
        barcode.RegisterModal(AfterBarcode);

    }
    
    private void AfterBarcode()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;

        if (Singleton.Instance.BarcodeType == ScanBarcodeType.Bonus)
        {
            appRuntime.SetTransition(Transition.ToTulipState);
        }
        else
        {
            appRuntime.SetTransition(Transition.ToHomeState);
        }
    }

    public override void OnLeave()
    {
        base.OnLeave();

        BarcodeModal barcode = BarcodeModal.Instance();
        barcode.UnRegisterModal();
        barcode.CloseModal();
    }
}