﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class GalleryState : FSMState
{
    public GalleryState()
    {
        stateID = StateID.GalleryState;
    }

    public override void OnEnter()
    {
        GalleryModal modal = GalleryModal.Instance();
        modal.OpenModal();
        modal.RegisterModal(ToHomeState);
    }

    private void ToHomeState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToHomeState);
    }

    public override void OnLeave()
    {
        GalleryModal modal = GalleryModal.Instance();
        modal.CloseModal();

        base.OnLeave();
    }
}