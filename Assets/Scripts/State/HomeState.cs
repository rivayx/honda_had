﻿using BaseApps;
using UnityEngine;

public class HomeState : FSMState
{
    public HomeState()
    {
        stateID = StateID.HomeState;
    }

    public override void OnEnter()
    {
        HomeModal modal = HomeModal.Instance();
        modal.OpenModal();
        modal.RegisterModal(StartAction, BarcodeAction);

        MenuModal menu = MenuModal.Instance();
        menu.OpenModal();
        menu.RegisterModal(ProfileAction, RundownAction, RulesAction, GalleryAction, EmergencyAction, LogoutAction, BarcodeAction);

        base.OnEnter();
    }

    private void StartAction()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToTulipState);
    }

    private void BarcodeAction()
    {
        if (Singleton.Instance.UserType == Role.Peserta)
            Singleton.Instance.BarcodeType = ScanBarcodeType.ReRegister;

        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToBarcodeState);
    }

    private void ProfileAction()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToProfileState);
    }

    private void RundownAction()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToRundownState);
    }

    private void RulesAction()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToRulesState);
    }

    private void GalleryAction()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToGalleryState);
    }

    private void EmergencyAction()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToEmergencyState);
    }

    private void LogoutAction()
    {
        MessageModal.Instance().OpenMessage("Apakah Anda yakin ingin keluar?", () => GetPostData.Logout(JSONActionLogout), true);
    }

    private void JSONActionLogout(string jsonString)
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToLoginState);
        PlayerPrefs.DeleteAll();
    }

    public override void OnLeave()
    {
        HomeModal modal = HomeModal.Instance();
        modal.UnRegisterModal();
        modal.CloseModal();

        MenuModal menu = MenuModal.Instance();
        menu.CloseModal();
        menu.UnRegisterModal();

        base.OnLeave();
    }
}
