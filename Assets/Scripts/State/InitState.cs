﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class InitState : FSMState
{
    public InitState()
    {
        stateID = StateID.InitState;
    }

    public override void OnEnter()
    {
        base.OnEnter();

        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToSplashState);
    }
}
