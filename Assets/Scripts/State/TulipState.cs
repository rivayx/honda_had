﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class TulipState : FSMState
{
    public TulipState()
    {
        stateID = StateID.TulipState;
    }

    public override void OnEnter()
    {
        base.OnEnter();
        Screen.orientation = ScreenOrientation.LandscapeLeft;

        TulipModal tulip = TulipModal.Instance();
        tulip.RegisterModal(ToHomeState, ToBarcodeState);
        tulip.OpenModal();
    }

    private void ToHomeState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToHomeState);
    }

    private void ToBarcodeState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToBarcodeState);
    }

    public override void OnLeave()
    {
        TulipModal tulip = TulipModal.Instance();
        tulip.CloseModal();
        tulip.UnRegisterModal();
        Screen.orientation = ScreenOrientation.Portrait;
        base.OnLeave();
    }
}