﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class RundownState : FSMState
{
    public RundownState()
    {
        stateID = StateID.RundownState;
    }

    public override void OnEnter()
    {
        base.OnEnter();

        RundownModal modal = RundownModal.Instance();
        modal.OpenModal();
        modal.RegisterModal(ToHomeState);
    }

    private void ToHomeState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToHomeState);
    }

    public override void OnLeave()
    {
        RundownModal modal = RundownModal.Instance();
        modal.CloseModal();

        base.OnLeave();
    }
}