﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class SplashState : FSMState
{
    public SplashState()
    {
        stateID = StateID.SplashState;
    }

    public override void OnEnter()
    {
        SplashModal modal = SplashModal.Instance();
        modal.FinishSplash = ToLoginState;
        modal.OpenModal();

        base.OnEnter();
    }


    private void ToLoginState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToLoginState);
    }

    public override void OnLeave()
    {
        SplashModal modal = SplashModal.Instance();
        modal.CloseModal();

        base.OnLeave();
    }
}