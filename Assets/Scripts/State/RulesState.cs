﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class RulesState : FSMState
{
    public RulesState()
    {
        stateID = StateID.RulesState;
    }

    public override void OnEnter()
    {
        RulesModal modal = RulesModal.Instance();
        modal.OpenModal();
        modal.RegisterModal(ToHomeState);
    }

    private void ToHomeState()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.ToHomeState);
    }

    public override void OnLeave()
    {
        RulesModal modal = RulesModal.Instance();
        modal.CloseModal();

        base.OnLeave();
    }
}