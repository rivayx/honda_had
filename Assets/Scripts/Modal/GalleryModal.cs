﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class GalleryModal : BaseModal
{
    [SerializeField] Button _Back;

    [SerializeField] Button _GalleryButton;
    [SerializeField] Button _InstagramButton;
    [SerializeField] Button _CameraButton;
    [SerializeField] Button _UploadButton;

    [SerializeField] GameObject _GalleryPanel;
    [SerializeField] GameObject _InstagramPanel;
    [SerializeField] GameObject _CameraPanel;
    [SerializeField] GameObject _UploadPanel;

    private enum Panel
    {
        Gallery,
        Instagram,
        Camera,
        Upload,
    }
    
    private static GalleryModal _Instance;
    public static GalleryModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<GalleryModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no GalleryModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        SwitchPanel(Panel.Gallery);
        _GalleryPanel.GetComponent<GalleryPanelControl>().OpenGallery();
    }

    public void RegisterModal(UnityAction backAction)
    {
        _Back.onClick.AddListener(backAction);

        _GalleryButton.onClick.AddListener(() => SwitchPanel(Panel.Gallery));
        _InstagramButton.onClick.AddListener(() => SwitchPanel(Panel.Instagram));
        _CameraButton.onClick.AddListener(() => SwitchPanel(Panel.Camera));
        _UploadButton.onClick.AddListener(() => SwitchPanel(Panel.Upload));
    }

    private void SwitchPanel(Panel panelShow)
    {
        _GalleryButton.GetComponentInChildren<Image>().color = Color.black;
        _GalleryButton.GetComponentInChildren<Text>().color = Color.black;
        _InstagramButton.GetComponentInChildren<Image>().color = Color.black;
        _InstagramButton.GetComponentInChildren<Text>().color = Color.black;
        _CameraButton.GetComponentInChildren<Image>().color = Color.black;
        _CameraButton.GetComponentInChildren<Text>().color = Color.black;
        _UploadButton.GetComponentInChildren<Image>().color = Color.black;
        _UploadButton.GetComponentInChildren<Text>().color = Color.black;

        _GalleryPanel.SetActive(false);
        _InstagramPanel.SetActive(false);
        _CameraPanel.SetActive(false);
        _UploadPanel.SetActive(false);

        switch (panelShow)
        {
            case Panel.Gallery:
                _GalleryButton.GetComponentInChildren<Image>().color = Color.red;
                _GalleryButton.GetComponentInChildren<Text>().color = Color.red;

                _GalleryPanel.SetActive(true);
                break;

            case Panel.Instagram:
                _InstagramButton.GetComponentInChildren<Image>().color = Color.red;
                _InstagramButton.GetComponentInChildren<Text>().color = Color.red;

                _InstagramPanel.SetActive(true);
                break;

            case Panel.Camera:
                _CameraButton.GetComponentInChildren<Image>().color = Color.red;
                _CameraButton.GetComponentInChildren<Text>().color = Color.red;

                _CameraPanel.SetActive(true);
                break;

            case Panel.Upload:
                _UploadButton.GetComponentInChildren<Image>().color = Color.red;
                _UploadButton.GetComponentInChildren<Text>().color = Color.red;

                _UploadPanel.SetActive(true);
                break;
        }
    }

    protected override void Loop()
    {
        base.Loop();

        
    }

    private void UnRegisterModal()
    {
        _Back.onClick.RemoveAllListeners();

        _GalleryButton.onClick.RemoveAllListeners();
        _InstagramButton.onClick.RemoveAllListeners();
        _CameraButton.onClick.RemoveAllListeners();
        _UploadButton.onClick.RemoveAllListeners();
    }

    public override void CloseModal()
    {
        _GalleryPanel.GetComponent<GalleryPanelControl>().CloseGallery();
        UnRegisterModal();
        base.CloseModal();
    }
}
