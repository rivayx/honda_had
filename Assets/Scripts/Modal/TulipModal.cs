﻿using System.Collections;
using BaseApps;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using SimpleJSON;

public class TulipModal : BaseModal
{
    [SerializeField] Button _Back;
    [SerializeField] RectTransform _TulipImage;
    [SerializeField] Button _BarcodeBonus;
    [SerializeField] Button _Giveup;
    [SerializeField] Button _Finish;

    [SerializeField] List<Sprite> ListSpriteBonus;
    [SerializeField] List<Sprite> ListSpriteFinish;

    private byte[] bytesPhoto;

    private UnityAction ToHomeState;

    private List<GameObject> ListTulipObject = new List<GameObject>();

    private float totalGeser;
    private float setengahGeser;
    private int TulipIndexShow;
    private int tempIndex;
    private bool isFinish;

    private int stage;

    private static TulipModal _Instance;
    public static TulipModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<TulipModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no TulipModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        isFinish = false;
        NativeToolkit.OnCameraShotComplete += CameraShotComplete;
        _TulipImage.gameObject.SetActive(false);
        totalGeser = -(_TulipImage.parent.GetComponent<HorizontalLayoutGroup>().spacing + _TulipImage.sizeDelta.x);
        setengahGeser = totalGeser / 2;

        Singleton.Instance.Loading = true;
        //JSONActionBarcodeType("{\"status\":\"10\"}");
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            JSONActionBarcodeType(PlayerPrefs.GetString("response_laststatus"));
        }
        else
        {
            GetPostData.ScanLastStatus(JSONActionBarcodeType);
        }
    }

    private void JSONActionBarcodeType(string jsonString)
    {
        Debug.Log(jsonString);


        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            if (jsonString == "")
            {
                MessageModal.Instance().OpenMessage("Gagal memuat gambar, silahkan cek koneksi internet anda", ToHomeState);
            }
        }

        JSONNode tulip = JSONNode.Parse(jsonString);

        Singleton.Instance.Loading = false;
        string status = tulip["status"];
        Debug.Log(status);
        switch (status)
        {
            case "0":
                Singleton.Instance.LastScanBarcode = ScanBarcodeType.Null;
                MessageModal.Instance().OpenMessage("Anda belum melakukan Pendaftaran Ulang, silahkan hubungi panitia untuk daftar ulang.", ToHomeState);
                break;

            case "5":
                Singleton.Instance.LastScanBarcode = ScanBarcodeType.ReRegister;
                MessageModal.Instance().OpenMessage("Panitia belum scan barcode untuk memulai STAGE 1, silahkan hubungi panitia.", ToHomeState);
                break;

            case "10":
                Singleton.Instance.LastScanBarcode = ScanBarcodeType.Start1;
                _Giveup.onClick.AddListener(() => MessageModal.Instance().OpenMessage("Apakah Anda akan menyerah?", GiveUpAction, true));
                stage = 1;

                PlayerPrefs.SetString("response_laststatus", jsonString);
                CreateUITulip();

                break;

            case "20":
                Singleton.Instance.LastScanBarcode = ScanBarcodeType.Finish1;
                _Giveup.onClick.AddListener(() => MessageModal.Instance().OpenMessage("Apakah Anda akan menyerah?", GiveUpAction, true));
                stage = 1;

                PlayerPrefs.SetString("response_laststatus", jsonString);
                CreateUITulip();

                break;

            case "30":
                Singleton.Instance.LastScanBarcode = ScanBarcodeType.Start2;
                _Giveup.onClick.AddListener(() => MessageModal.Instance().OpenMessage("Apakah Anda akan menyerah?", GiveUpAction, true));
                stage = 2;

                PlayerPrefs.SetString("response_laststatus", jsonString);
                CreateUITulip();

                break;

            case "40":
                Singleton.Instance.LastScanBarcode = ScanBarcodeType.Finish2;
                _Giveup.onClick.AddListener(() => MessageModal.Instance().OpenMessage("Apakah Anda akan menyerah?", GiveUpAction, true));
                stage = 2;

                PlayerPrefs.SetString("response_laststatus", jsonString);
                CreateUITulip();

                break;

            case null:
                MessageModal.Instance().OpenMessage("Response tidak dikenal", ToHomeState);
                break;

            default:
                JSONActionBarcodeType(PlayerPrefs.GetString("response_laststatus"));
                break;
        }
        Debug.Log(Singleton.Instance.LastScanBarcode);


        if (tempIndex != 0)
        {
            TulipIndexShow = tempIndex;
            _TulipImage.parent.GetComponent<RectTransform>().anchoredPosition = new Vector2(-(_TulipImage.parent.GetComponent<HorizontalLayoutGroup>().spacing + _TulipImage.sizeDelta.x) * TulipIndexShow,
                                                                                            _TulipImage.parent.GetComponent<RectTransform>().anchoredPosition.y);

        }
    }

    private void GiveUpAction()
    {
        GetPostData.GiveUp(stage, JSONActionGiveUp);

        if (stage == 1)
        {
            Application.OpenURL(URL.LOCATION_STAGE1);
        }
        else
        {
            Application.OpenURL(URL.LOCATION_STAGE2);
        }
    }

    private void JSONActionGiveUp(string jsonString)
    {
        Debug.Log("GiveUp :: " + jsonString);
    }

    public void RegisterModal(UnityAction backAction, UnityAction barcodeAction)
    {
        ToHomeState = backAction;
        _BarcodeBonus.onClick.AddListener(() => Singleton.Instance.BarcodeType = ScanBarcodeType.Bonus);
        _BarcodeBonus.onClick.AddListener(barcodeAction);
        _BarcodeBonus.onClick.AddListener(() => tempIndex = TulipIndexShow);
        _Finish.onClick.AddListener(NativeToolkit.TakeCameraShot);
        _Finish.onClick.AddListener(() => isFinish = true);

        _Back.onClick.AddListener(backAction);

    }

    private void CreateUITulip()
    {
        Debug.Log(StaticFunction.CountFileTulip(Singleton.Instance.Class, stage));
        for (int i = 1; i <= StaticFunction.CountFileTulip(Singleton.Instance.Class, stage); i++)
        {
            GameObject newTulip = Instantiate(_TulipImage.gameObject, _TulipImage.parent);
            newTulip.SetActive(true);
            newTulip.name = "Tulip - " + i.ToString("D2");

            ListTulipObject.Add(newTulip);
        }
    }

    void CameraShotComplete(Texture2D img, string path)
    {
        GetPostData.UploadGallery(img.EncodeToPNG(), stage, JSONActionUploadPhoto);
    }

    private void JSONActionUploadPhoto(string jsonString)
    {
        Singleton.Instance.Loading = false;
        Debug.Log("Upload Photo : " + jsonString);

        JSONNode upload = JSONNode.Parse(jsonString);

        bool success = upload["status"].AsBool;
        if (success)
        {
            MessageModal.Instance().OpenMessage("Sukses mengupload foto");
            isFinish = false;
        }
        else
        {
            MessageModal.Instance().OpenMessage(upload["data"]);
        }
    }

    protected override void Loop()
    {
        base.Loop();


        if (ListTulipObject.Count > 0)
        {
            foreach (GameObject go in ListTulipObject)
            {
                go.GetComponent<Image>().sprite = null;
            }

            ListTulipObject[TulipIndexShow].GetComponent<Image>().sprite = StaticFunction.LoadTulipSprite(Singleton.Instance.Class, stage, TulipIndexShow + 1);

            if (TulipIndexShow < ListTulipObject.Count - 1)
                ListTulipObject[TulipIndexShow + 1].GetComponent<Image>().sprite = StaticFunction.LoadTulipSprite(Singleton.Instance.Class, stage, TulipIndexShow + 2);

            if (TulipIndexShow > 0)
                ListTulipObject[TulipIndexShow - 1].GetComponent<Image>().sprite = StaticFunction.LoadTulipSprite(Singleton.Instance.Class, stage, TulipIndexShow);

            if ((totalGeser * TulipIndexShow) + setengahGeser > _TulipImage.parent.localPosition.x)
            {
                TulipIndexShow++;
            }
            if ((totalGeser * TulipIndexShow) - setengahGeser < _TulipImage.parent.localPosition.x)
            {
                TulipIndexShow--;
            }

            float anchoredX = Mathf.Lerp(_TulipImage.parent.GetComponent<RectTransform>().anchoredPosition.x,
                                         -(_TulipImage.parent.GetComponent<HorizontalLayoutGroup>().spacing + _TulipImage.sizeDelta.x) * TulipIndexShow,
                                         Time.deltaTime * 15);

            if (Singleton.Instance.startLerp)
                _TulipImage.parent.GetComponent<RectTransform>().anchoredPosition = new Vector2(anchoredX, _TulipImage.parent.GetComponent<RectTransform>().anchoredPosition.y);

            foreach (Sprite bonus in ListSpriteBonus)
            {
                if (ListTulipObject[TulipIndexShow].GetComponent<Image>().sprite == bonus)
                {
                    _BarcodeBonus.gameObject.SetActive(true);
                    break;
                }
                else
                {
                    _BarcodeBonus.gameObject.SetActive(false);

                    foreach (Sprite finish in ListSpriteFinish)
                    {
                        if (ListTulipObject[TulipIndexShow].GetComponent<Image>().sprite == finish)
                        {
                            _Finish.gameObject.SetActive(true);
                            break;
                        }
                        else
                        {
                            _Finish.gameObject.SetActive(false);
                            _Giveup.gameObject.SetActive(true);
                        }
                    }
                }
            }
        }
    }

    public void UnRegisterModal()
    {
        _BarcodeBonus.onClick.RemoveAllListeners();
        _Giveup.onClick.RemoveAllListeners();
        _Back.onClick.RemoveAllListeners();
        _Finish.onClick.RemoveAllListeners();
    }

    public override void CloseModal()
    {
        NativeToolkit.OnCameraShotComplete -= CameraShotComplete;
        if (ListTulipObject.Count != 0)
        {
            foreach (GameObject go in ListTulipObject)
            {
                Destroy(go);
            }
            ListTulipObject.Clear();
        }

        base.CloseModal();
    }
}
