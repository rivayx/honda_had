﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RundownModal : BaseModal
{
    [SerializeField] Button _Back;
    [SerializeField] List<GameObject> ListTulipObject;


    private float totalGeser;
    private float setengahGeser;
    private int RundownShowIndex;

    private static RundownModal _Instance;
    public static RundownModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<RundownModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no RundownModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        totalGeser = -(ListTulipObject[0].transform.parent.GetComponent<HorizontalLayoutGroup>().spacing + ListTulipObject[0].GetComponent<RectTransform>().sizeDelta.x);
        setengahGeser = totalGeser / 2;

        RundownShowIndex = 0;
    }

    public void RegisterModal(UnityAction backAction)
    {
        _Back.onClick.AddListener(backAction);
    }

    protected override void Loop()
    {
        base.Loop();

        if (ListTulipObject.Count > 0)
        {
            
            if ((totalGeser * RundownShowIndex) + setengahGeser > ListTulipObject[0].transform.parent.localPosition.x)
                RundownShowIndex++;

            if ((totalGeser * RundownShowIndex) - setengahGeser < ListTulipObject[0].transform.parent.localPosition.x)
                RundownShowIndex--;


            float anchoredX = Mathf.Lerp(ListTulipObject[0].transform.parent.GetComponent<RectTransform>().anchoredPosition.x,
                                         -(ListTulipObject[0].transform.parent.GetComponent<HorizontalLayoutGroup>().spacing + ListTulipObject[0].GetComponent<RectTransform>().sizeDelta.x) * RundownShowIndex,
                                         Time.deltaTime * 20);

            if (Singleton.Instance.startLerp)
                ListTulipObject[0].transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector2(anchoredX, ListTulipObject[0].transform.parent.GetComponent<RectTransform>().anchoredPosition.y);

        }
    }

    private void UnregisterModal()
    {
        _Back.onClick.RemoveAllListeners();
    }

    public override void CloseModal()
    {
        UnregisterModal();

        base.CloseModal();
    }
}
