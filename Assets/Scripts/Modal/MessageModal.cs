﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MessageModal : BaseModal
{
    [SerializeField]
    private Text _MessageText;
    [SerializeField]
    private Text _Value; //for loading
    [SerializeField]
    private Button _OK;
    [SerializeField]
    private Button _Cancel;

    private static MessageModal _Instance;
    public static MessageModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<MessageModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no MessageModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        transform.SetAsLastSibling();
        base.OpenModal();
        _OK.onClick.RemoveAllListeners();
        _Cancel.onClick.RemoveAllListeners();

        _MessageText.alignment = TextAnchor.UpperLeft;
        _Value.gameObject.SetActive(false);
        _OK.gameObject.SetActive(true);
        _Cancel.gameObject.SetActive(false);
    }

    public void Loading(float val)
    {
        OpenModal();
        _Value.gameObject.SetActive(true);
        _MessageText.text = "LOADING...";
        _Value.text = ((int)(val * 100)).ToString() + " %";
        _MessageText.alignment = TextAnchor.MiddleCenter;
        _OK.onClick.AddListener(CloseModal);

        _OK.gameObject.SetActive(false);
    }

    public void OpenMessage(string message)
    {
        OpenModal();
        _MessageText.text = message;
        _OK.onClick.AddListener(CloseModal);
    }

    public void OpenMessage(string message, UnityAction okAction)
    {
        OpenModal();
        _MessageText.text = message;
        _OK.onClick.AddListener(okAction);

    }

    public void OpenMessage(string message, UnityAction okAction, UnityAction cancelAction)
    {
        OpenModal();
        _Cancel.gameObject.SetActive(true);
        _MessageText.text = message;
        _OK.onClick.AddListener(okAction);
        _Cancel.onClick.AddListener(cancelAction);

    }

    public void OpenMessage(string message, UnityAction okAction, bool cancel)
    {
        OpenModal();
        _Cancel.gameObject.SetActive(cancel);
        _MessageText.text = message;
        _OK.onClick.AddListener(okAction);
        _Cancel.onClick.AddListener(CloseModal);

    }

    public override void CloseModal()
    {
        _MessageText.text = "";
        _OK.onClick.RemoveAllListeners();
        _Cancel.onClick.RemoveAllListeners();

        base.CloseModal();
    }
}
