﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using SimpleJSON;

public class ChangePasswordModal : BaseModal
{
    [SerializeField] Button _Back;
    [SerializeField] InputField _OldPassword;
    [SerializeField] InputField _NewPassword;
    [SerializeField] InputField _ConfirmPassword;
    [SerializeField] Button _Update;

    private UnityAction ToProfileState;

    private static ChangePasswordModal _Instance;
    public static ChangePasswordModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<ChangePasswordModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no ChangePasswordModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();

        _OldPassword.text = "";
        _NewPassword.text = "";
        _ConfirmPassword.text = "";
    }

    public void RegisterModal(UnityAction backAction)
    {
        ToProfileState = backAction;
        _Back.onClick.AddListener(backAction);
        _Update.onClick.AddListener(UpdateAction);
    }

    private void UpdateAction()
    {
        if (_NewPassword.text != _ConfirmPassword.text)
            MessageModal.Instance().OpenMessage("Password yang anda masukan tidak sesuai");
        else
            GetPostData.ChangePassword(_OldPassword.text, _NewPassword.text, _ConfirmPassword.text, JSONActionChangePassword);
    }

    private void JSONActionChangePassword(string jsonString)
    {
        JSONNode password = JSONNode.Parse(jsonString);
        Singleton.Instance.Loading = false;

        string message = password["msg"];

        if (!string.IsNullOrEmpty(message))
        {
            MessageModal.Instance().OpenMessage(message);
        }
        else
        {
            MessageModal.Instance().OpenMessage("Sukses merubah password", ToProfileState);
        }
    }

    private void UnregisterModal()
    {
        _Back.onClick.RemoveAllListeners();
        _Update.onClick.RemoveAllListeners();
    }

    public override void CloseModal()
    {
        UnregisterModal();

        base.CloseModal();
    }
}