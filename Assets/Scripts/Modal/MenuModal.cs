﻿using System.Collections;
using System.Collections.Generic;
using BaseApps;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class MenuModal : BaseModal
{
    [SerializeField] Button _Profile;

    [SerializeField] Text _Number;
    [SerializeField] Text _Name;
    [SerializeField] Text _Email;

    [SerializeField] GameObject _MenuPeserta;
    [SerializeField] GameObject _MenuPanitia;

    //PESERTA
    [SerializeField] Button _Rundown;
    [SerializeField] Button _Rules;
    [SerializeField] Button _Gallery;
    [SerializeField] Button _Emergency;
    [SerializeField] Button _Logout;

    //PANITIA
    [SerializeField] Button _Start1;
    [SerializeField] Button _Finish1;
    [SerializeField] Button _Start2;
    [SerializeField] Button _Finish2;

    [SerializeField] Button _MenuBackground;
    [SerializeField] Transform _MenuPanel;
    [SerializeField] Transform _HideTransform;
    [SerializeField] Transform _ShowTransform;
    
    public bool ShowMenu { get; set; }

    private static MenuModal _Instance;
    public static MenuModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<MenuModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no MenuModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();

        _MenuPanitia.SetActive(false);
        _MenuPeserta.SetActive(false);

        switch (Singleton.Instance.UserType)
        {
            case Role.Panitia:
                _MenuPanitia.SetActive(true);
                break;
            case Role.Peserta:
                _MenuPeserta.SetActive(true);
                break;
        }

        ShowMenu = false;
    }

    public void RegisterModal(UnityAction profileAction, UnityAction rundownAction, UnityAction rulesAction, UnityAction galleryAction, UnityAction emergencyAction, UnityAction LogoutAction, UnityAction barcodeAction)
    {
        _Start1.onClick.AddListener(() => Singleton.Instance.BarcodeType = ScanBarcodeType.Start1);
        _Finish1.onClick.AddListener(() => Singleton.Instance.BarcodeType = ScanBarcodeType.Finish1);
        _Start2.onClick.AddListener(() => Singleton.Instance.BarcodeType = ScanBarcodeType.Start2);
        _Finish2.onClick.AddListener(() => Singleton.Instance.BarcodeType = ScanBarcodeType.Finish2);

        _Start1.onClick.AddListener(barcodeAction);
        _Finish1.onClick.AddListener(barcodeAction);
        _Start2.onClick.AddListener(barcodeAction);
        _Finish2.onClick.AddListener(barcodeAction);


        _Profile.onClick.AddListener(profileAction);
        _Rundown.onClick.AddListener(rundownAction);
        _Rules.onClick.AddListener(rulesAction);
        _Gallery.onClick.AddListener(galleryAction);
        _Emergency.onClick.AddListener(emergencyAction);
        _Logout.onClick.AddListener(LogoutAction);
        _MenuBackground.onClick.AddListener(() => ShowMenu = false);
    }

    protected override void Loop()
    {
        base.Loop();

        _Number.text = Singleton.Instance.Number;
        _Name.text = Singleton.Instance.ParticipantName;
        _Email.text = Singleton.Instance.Email;

        if (ShowMenu)
        {
            _MenuPanel.position = Vector3.Lerp(_MenuPanel.position, _ShowTransform.position, Time.deltaTime * 8);
            _MenuBackground.gameObject.SetActive(true);
        }
        else
        {
            FindObjectOfType<EventSystem>().SetSelectedGameObject(null);
            _MenuPanel.position = Vector3.Lerp(_MenuPanel.position, _HideTransform.position, Time.deltaTime * 8);
            _MenuBackground.gameObject.SetActive(false);
        }
    }

    public void UnRegisterModal()
    {
        _Start1.onClick.RemoveAllListeners();
        _Finish1.onClick.RemoveAllListeners();
        _Start2.onClick.RemoveAllListeners();
        _Finish2.onClick.RemoveAllListeners();

        _Profile.onClick.RemoveAllListeners();
        _Rundown.onClick.RemoveAllListeners();
        _Rules.onClick.RemoveAllListeners();
        _Gallery.onClick.RemoveAllListeners();
        _Emergency.onClick.RemoveAllListeners();
        _Logout.onClick.RemoveAllListeners();
        _MenuBackground.onClick.RemoveAllListeners();
    }

    public override void CloseModal()
    {
        UnRegisterModal();
        base.CloseModal();
    }
}
