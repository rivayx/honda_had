﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SplashModal : BaseModal
{
    public UnityAction FinishSplash { get; set; }

    private static SplashModal _Instance;
    public static SplashModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<SplashModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no SplashModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        StartCoroutine(WaitSplash());
    }

    private IEnumerator WaitSplash()
    {
        yield return new WaitForEndOfFrame();
        if (!_ModalPanel.activeSelf)
            _ModalPanel.SetActive(true);
        yield return new WaitForSeconds(3f);
        FinishSplash();
    }

}
