﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class EmergencyModal : BaseModal
{
    [SerializeField] Button _Back;

    private static EmergencyModal _Instance;
    public static EmergencyModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<EmergencyModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no EmergencyModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
    }

    public void RegisterModal(UnityAction backAction)
    {
        _Back.onClick.AddListener(backAction);
    }

    public void CallAction(string phoneNumber)
    {
        Application.OpenURL("tel:" + phoneNumber);
    }

    private void UnRegisterModal()
    {
        _Back.onClick.RemoveAllListeners();
    }

    public override void CloseModal()
    {
        UnRegisterModal();
        base.CloseModal();
    }
}
