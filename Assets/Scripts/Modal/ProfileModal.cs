﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ProfileModal : BaseModal
{
    [SerializeField] Button _Back;
    [SerializeField] Image _ProfileImage; 
    [SerializeField] InputField _Name;
    [SerializeField] Button _Update;
    [SerializeField] Button _ChangePassword;

    private static ProfileModal _Instance;
    public static ProfileModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<ProfileModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no ProfileModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();

        _Name.text = Singleton.Instance.ParticipantName;
    }

    public void RegisterModal(UnityAction backAction, UnityAction changePassword)
    {
        _Back.onClick.AddListener(backAction);
        _Update.onClick.AddListener(UpdateAction);
        _ChangePassword.onClick.AddListener(changePassword);
    }

    private void UpdateAction()
    {
        PlayerPrefs.SetString("name", _Name.text);
        Singleton.Instance.ParticipantName = PlayerPrefs.GetString("name");

        MessageModal.Instance().OpenMessage("Sukses merubah data");
    }

    private void UnregisterModal()
    {
        _Back.onClick.RemoveAllListeners();
        _Update.onClick.RemoveAllListeners();
        _ChangePassword.onClick.RemoveAllListeners();
    }

    public override void CloseModal()
    {
        UnregisterModal();
        base.CloseModal();
    }
}
