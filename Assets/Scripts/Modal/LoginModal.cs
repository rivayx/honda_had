﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using SimpleJSON;
using BaseApps;

public class LoginModal : BaseModal
{
    [SerializeField] InputField _Email;
    [SerializeField] InputField _Password;
    [SerializeField] Button _SignIn;
    [SerializeField] Button _SignUp;

    private UnityAction ToHomeState;

    private bool cache;

    private static LoginModal _Instance;
    public static LoginModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<LoginModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no LoginModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        //PlayerPrefs.DeleteAll();

        //PlayerPrefs.SetString("email", "peserta3@example.com");
        //PlayerPrefs.SetString("password", "had2019");
        Debug.Log(PlayerPrefs.GetString("email"));
        cache = false;
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString("email")))
        {
            Singleton.Instance.Loading = true;
            cache = true;

            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                StartCoroutine(WaitToHomeState());
            }
            else
            {
                GetPostData.Login(PlayerPrefs.GetString("email"), PlayerPrefs.GetString("password"), JSONActionLogin);
            }
        }
        else
        {
            base.OpenModal();
        }
    }
   

    private IEnumerator WaitToHomeState()
    {
        yield return new WaitForEndOfFrame();
        yield return ToHomeState != null;
        Singleton.Instance.Loading = false;
        Singleton.Instance.UserType = Role.Peserta;
        ToHomeState();
    }

    public void RegisterModal(UnityAction toHomeState)
    {
        ToHomeState = toHomeState;
        _SignIn.onClick.AddListener(LoginAction);
        _SignUp.onClick.AddListener(() => Application.OpenURL(URL.REGISTER));
    }

    private void LoginAction()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            MessageModal.Instance().OpenMessage("Tidak ada koneksi Internet");
        }
        else
        {

            //JSONActionLogin("");
            Singleton.Instance.Loading = true;

            cache = false;
            GetPostData.Login(_Email.text, _Password.text, JSONActionLogin);
        }
    }

    private void JSONActionLogin(string jsonString)
    {
        //Debug.Log(jsonString);
        Singleton.Instance.Loading = false;

        JSONNode login = JSONNode.Parse(jsonString);

        bool success = login["isLoggedIn"].AsBool;
        if (success)
        {
            string pname = login["name"];
            string changePass = login["changePass"];
            string key = login["key"];
            string number = login["number"];
            string role = login["role"];
            string kelas = login["class"];

            if (string.IsNullOrEmpty(PlayerPrefs.GetString("name")))
                PlayerPrefs.SetString("name", pname);

            Singleton.Instance.ParticipantName = PlayerPrefs.GetString("name");

            Singleton.Instance.ChangePass = changePass == "1" ? true : false;
            Singleton.Instance.Key = key;
            Singleton.Instance.Number = number;
            Singleton.Instance.UserType = role == "2" ? Role.Panitia : Role.Peserta;
            Singleton.Instance.Class = kelas == "wing" ? Class.Wing : Class.BigWing;

            PlayerPrefs.SetString("kelas", kelas);
            Debug.Log(Singleton.Instance.Key);

            //if (Singleton.Instance.UserType == Role.Peserta)
            if (!cache)
            {
                MessageModal.Instance().OpenMessage("Login sukses", ToHomeState);

                PlayerPrefs.SetString("email", _Email.text);
                PlayerPrefs.SetString("password", _Password.text);
            }
            else
            {
                ToHomeState();
            }
        }
        else
        {
            if (!cache)
                MessageModal.Instance().OpenMessage("Login Gagal");
        }
    }

    public void UnRegisterModal()
    {
        _SignIn.onClick.RemoveAllListeners();
        _SignUp.onClick.RemoveAllListeners();
    }

    public override void CloseModal()
    {
        _Email.text = "";
        _Password.text = "";
        base.CloseModal();
    }
}
