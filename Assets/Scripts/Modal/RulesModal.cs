﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class RulesModal : BaseModal
{
    [SerializeField] Button _Back;
    [SerializeField] RectTransform _RulesImage;

    private List<GameObject> ListRulesObject = new List<GameObject>();

    private float totalGeser;
    private float setengahGeser;
    private int RulesIndexShow;

    private static RulesModal _Instance;
    public static RulesModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<RulesModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no RulesModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        _RulesImage.gameObject.SetActive(false);
        totalGeser = -(_RulesImage.parent.GetComponent<HorizontalLayoutGroup>().spacing + _RulesImage.sizeDelta.x);
        setengahGeser = totalGeser / 2;

        for (int i = 1; i <= 48; i++)
        {
            GameObject newTulip = Instantiate(_RulesImage.gameObject, _RulesImage.parent);
            newTulip.SetActive(true);
            newTulip.name = "Rules - " + i.ToString("D2");

            ListRulesObject.Add(newTulip);
        }
    }

    public void RegisterModal(UnityAction backAction)
    {
        _Back.onClick.AddListener(backAction);
    }

    protected override void Loop()
    {
        base.Loop();


        if (ListRulesObject.Count > 0)
        {
            foreach (GameObject go in ListRulesObject)
            {
                go.GetComponent<Image>().sprite = null;
            }

            ListRulesObject[RulesIndexShow].GetComponent<Image>().sprite = StaticFunction.LoadRulesSprite(RulesIndexShow + 1);

            if (RulesIndexShow < ListRulesObject.Count - 1)
                ListRulesObject[RulesIndexShow + 1].GetComponent<Image>().sprite = StaticFunction.LoadRulesSprite(RulesIndexShow + 2);

            if (RulesIndexShow > 0)
                ListRulesObject[RulesIndexShow - 1].GetComponent<Image>().sprite = StaticFunction.LoadRulesSprite(RulesIndexShow);


            if ((totalGeser * RulesIndexShow) + setengahGeser > _RulesImage.parent.localPosition.x)
                RulesIndexShow++;

            if ((totalGeser * RulesIndexShow) - setengahGeser < _RulesImage.parent.localPosition.x)
                RulesIndexShow--;


            float anchoredX = Mathf.Lerp(_RulesImage.parent.GetComponent<RectTransform>().anchoredPosition.x,
                                         -(_RulesImage.parent.GetComponent<HorizontalLayoutGroup>().spacing + _RulesImage.sizeDelta.x) * RulesIndexShow,
                                         Time.deltaTime * 20);

            if (Singleton.Instance.startLerp)
                _RulesImage.parent.GetComponent<RectTransform>().anchoredPosition = new Vector2(anchoredX, _RulesImage.parent.GetComponent<RectTransform>().anchoredPosition.y);

        }
    }

    private void UnRegisterModal()
    {
        _Back.onClick.RemoveAllListeners();
    }

    public override void CloseModal()
    {
        if (ListRulesObject.Count != 0)
        {
            foreach (GameObject go in ListRulesObject)
            {
                Destroy(go);
            }
            ListRulesObject.Clear();
        }
        UnRegisterModal();
        base.CloseModal();
    }
}
