﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using SimpleJSON;
using BaseApps;

public class HomeModal : BaseModal
{
    [SerializeField] GameObject _Panitia;
    [SerializeField] GameObject _Peserta;

    [SerializeField] Button _Menu;
    [SerializeField] Button _Barcode;
    [SerializeField] Button _Start;

    private static HomeModal _Instance;
    public static HomeModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<HomeModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no Modal2 in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();




        _Panitia.SetActive(false);
        _Peserta.SetActive(false);

        switch (Singleton.Instance.UserType)
        {
            case Role.Panitia:
                _Panitia.SetActive(true);
                _Barcode.gameObject.SetActive(false);
                break;
            case Role.Peserta:
                _Peserta.SetActive(true);
                _Barcode.gameObject.SetActive(true);
                break;
        }

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Debug.Log("No Connection Internet");
            Singleton.Instance.Class = PlayerPrefs.GetString("kelas") == "wing" ? Class.Wing : Class.BigWing;
        }
        else
        {
            GetPostData.UserInfo(JSONActionUserInfo);
        }

    }



    private void JSONActionUserInfo(string jsonString)
    {

        JSONNode login = JSONNode.Parse(jsonString);

        Singleton.Instance.Email = login["email"];
        Singleton.Instance.MobilePhone = login["mobile"];

#if !UNITY_EDITOR
        if (!Input.location.isEnabledByUser)
        {
            MessageModal.Instance().OpenMessage("Mohon aktifkan GPS Anda");

        }
#endif
    }
    
    public void RegisterModal(UnityAction startAction, UnityAction barcodeAction)
    {
        _Menu.onClick.AddListener(MenuOpen);
        _Start.onClick.AddListener(startAction);
        _Barcode.onClick.AddListener(barcodeAction);
    }

    public void MenuOpen()
    {
        MenuModal.Instance().ShowMenu = true;
    }

    protected override void Loop()
    {
        base.Loop();
        if (Input.GetKey(KeyCode.Escape))
        {
            if (MenuModal.Instance().ShowMenu)
            {
                MenuModal.Instance().ShowMenu = false;
            }
        }
    }

    public void UnRegisterModal()
    {
        _Menu.onClick.RemoveAllListeners();
        _Start.onClick.RemoveAllListeners();
        _Barcode.onClick.RemoveAllListeners();
    }

    public override void CloseModal()
    {
        base.CloseModal();
    }
}
