﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using BaseApps;
using SimpleJSON;

public class BarcodeModal : BaseModal
{
    [SerializeField] Button _Back;
    [SerializeField] Text _Title;


    [SerializeField] QRCodeDecodeController e_qrController;

    private UnityAction ScanSuccess;

    private bool ScanFound;
    private string ResultBonus;

    public bool isBonus { get; set; }

    private static BarcodeModal _Instance;
    public static BarcodeModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<BarcodeModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no BarcodeModal in the system");
            }
        }
        return _Instance;
    }

    void Start()
    {
        e_qrController.onQRScanFinished += new QRCodeDecodeController.QRScanFinished(this.ResultText);
    }

    public override void OpenModal()
    {
        base.OpenModal();

        NativeToolkit.OnCameraShotComplete += CameraShotComplete;
        e_qrController.Reset();
        e_qrController.StartWork();

        ScanFound = false;
        isBonus = false;
        switch (Singleton.Instance.BarcodeType)
        {
            case ScanBarcodeType.ReRegister:
                _Title.text = "Scan Daftar Ulang";
                break;
            case ScanBarcodeType.Bonus:
                isBonus = true;
                _Title.text = "Scan Bonus";
                break;
            case ScanBarcodeType.Start1:
                _Title.text = "Scan Start Stage 1";
                break;
            case ScanBarcodeType.Finish1:
                _Title.text = "Scan Finish Stage 1";
                break;
            case ScanBarcodeType.Start2:
                _Title.text = "Scan Start Stage 2";
                break;
            case ScanBarcodeType.Finish2:
                _Title.text = "Scan Finish Stage 2";
                break;
        }

    }

    private void ResultText(string result)
    {
        Debug.Log(result);

        if (Singleton.Instance.BarcodeType == ScanBarcodeType.Bonus)
        {
            ResultBonus = result;

        }

        switch (Singleton.Instance.UserType)
        {
            case Role.Peserta:
                if (!ScanFound)
                {
                    ScanFound = true;
                    GetPostData.ScanPeserta(result, JSONActionScan);
                }
                break;

            case Role.Panitia:
                if (!ScanFound)
                {
                    ScanFound = true;
                    GetPostData.ScanPanitia(result, JSONActionScan);
                }
                break;
        }

    }

    public void RegisterModal(UnityAction backAction)
    {
        _Back.onClick.AddListener(backAction);
        ScanSuccess = backAction;
    }


    private void JSONActionScan(string jsonString)
    {
        Debug.Log("Barcode ::\n" + jsonString);
        JSONNode login = JSONNode.Parse(jsonString);

        bool success = login["status"].AsBool;
        if (success)
        {
            MessageModal.Instance().OpenMessage("Scan Sukses", BarcodeScaned);
        }
        else
        {
            ScanFound = false;
        }
    }

    private void BarcodeScaned()
    {
        if (isBonus)
        {
#if UNITY_EDITOR
            Debug.Log("asdasdasdasdasdasd");
            JSONActionUploadPhoto("{\"status\":\"true\"}");

            #else
            NativeToolkit.TakeCameraShot();
#endif
        }
        else
        {
            ScanSuccess();
        }
    }

    void CameraShotComplete(Texture2D img, string path)
    {
        GetPostData.UploadGallery(img.EncodeToPNG(), ResultBonus, JSONActionUploadPhoto);
    }

    private void JSONActionUploadPhoto(string jsonString)
    {
        Singleton.Instance.Loading = false;
        Debug.Log("Upload Photo : " + jsonString);

        JSONNode upload = JSONNode.Parse(jsonString);

        bool success = upload["status"].AsBool;
        if (success)
        {
            MessageModal.Instance().OpenMessage("Sukses mengupload foto", ScanSuccess);
        }
        else
        {
            MessageModal.Instance().OpenMessage(upload["data"]);
        }
    }

    protected override void Loop()
    {
        base.Loop();
    }

    public void UnRegisterModal()
    {
        _Back.onClick.RemoveAllListeners();
    }

    public override void CloseModal()
    {
        NativeToolkit.OnCameraShotComplete -= CameraShotComplete;
        e_qrController.StopWork();

        base.CloseModal();
    }
}
